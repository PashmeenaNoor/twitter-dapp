import React from "react";
import Leftbar from "./partials/leftbar";
import AddTweet from "./partials/addtweet";
import TweetListings from "./partials/TweetsListings";
import Search from "./partials/search";
import Trends from "./partials/trends";

function Twitter (){
return(
    <>
        <div className="twitter-main">
            <Leftbar></Leftbar>  
            <div className="center">
                <AddTweet></AddTweet>
                <TweetListings></TweetListings>
            </div>
            <div className="rightbar">
                <Search></Search>
                <Trends></Trends>
            </div>
        </div>
    </>
)}


export default Twitter;