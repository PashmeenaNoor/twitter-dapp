import React from "react";

function Trends (){
    return (
        <>
            <div class="trendings">
                <h3>Trends for you</h3>
                <a href="#!" title="">
                    <strong>#Web3nft</strong>
                    <span>125k tweets</span>
                </a>
                <a href="#!" title="">
                    <strong>#AhadRazaMir</strong>
                    <span>100k tweets</span>
                </a>
                <a href="#!" title="">
                    <strong>#Beauty</strong>
                    <span>98k tweets</span>
                </a>
                <a href="#!" title="">
                    <strong>#FloodsInPakistan2022</strong>
                    <span>53k tweets</span>
                </a>
                <a href="#!" title="">
                    <strong>#IndvsPak</strong>
                    <span>42k tweets</span>
                </a>
                <a href="#!" title="">
                    <strong>#AsiaCup2022</strong>
                    <span>22k tweets</span>
                </a>
            </div>
        </>
    )
}

export default Trends;