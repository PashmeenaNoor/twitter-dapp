import React from "react";

function AddTweet (){
    return (
        <>
            <div className="add-tweet">
                <img src="images/teacher.webp" alt=""></img>
                <div className="tweet-form">
                    <form action="">
                        <textarea name="" id="" cols="" rows="3" placeholder="Add your tweet"></textarea>
                        <button className="btn twitter-bg">Tweet</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default AddTweet;