import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './login';
import Twitter from './twitter';

function App() {
  return (
    <>
      <Router>
        <Routes>
            <Route exact path="/" element={<Login />}>
            <Route exact path="/twitter" element={<Twitter />} />
          </Route>
        </Routes>
      </Router>
      <Login></Login>
      </>    
  );
}

export default App;
